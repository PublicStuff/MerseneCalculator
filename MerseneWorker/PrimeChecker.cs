﻿namespace MerseneWorker
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Numerics;

    /// <summary>
    /// The static prime checker class, logic copied from : https://rosettacode.org/wiki/Lucas-Lehmer_test#C.23
    /// </summary>
    public static class PrimeChecker
    {
        static BigInteger ZERO = new BigInteger(0);
        static BigInteger ONE = new BigInteger(1);
        static BigInteger TWO = new BigInteger(2);
        static BigInteger FOUR = new BigInteger(4);

        public static bool IsMersennePrime(int p)
        {
            if (p % 2 == 0) return false;
            else
            {
                for (int i = 3; i <= (int)Math.Sqrt(p); i += 2)
                    if (p % i == 0) return false; //not prime
                BigInteger m_p = BigInteger.Pow(TWO, p) - ONE;
                BigInteger s = FOUR;
                for (int i = 3; i <= p; i++)
                    s = (s * s - TWO) % m_p;
                return s == ZERO;
            }
        }

        public static IEnumerable<int> GetMersenePrimesBetween(int from, int to)
        {
            if (from > to)
            {
                return new List<int>();
            }

            from = Math.Max(from, 2);
            to = Math.Max(to, 2);

            BigInteger two = new BigInteger(2);

            return Enumerable.Range(from, to - 1).Where(bigNum => IsMersennePrime(bigNum));

            //var res2 = Enumerable.Range(from, to - 1).Select(num => BigInteger.Pow(two, num) - 1);
            //return res2.Where(bigNum => IsPrime(bigNum));

            //var res1 = Enumerable.Range(from, to - 1);
            //var res2 = res1.Select(num => BigInteger.Pow(two, num) - 1);
            //return res2.Where(bigNum => IsPrime(bigNum));


            //return Enumerable.Range(from, to - 1)
            //                 .Select(num => BigInteger.Pow(two, num) - 1)
            //                 .Where(bigNum => IsPrime(bigNum));
        }
    }
}
