﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace MerseneWorker
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args == null || args.Length != 2)
            {
                //Console.WriteLine("Error:");
                //Console.WriteLine("Invalid arguments!");
                Environment.Exit(1);
            }

            int from = 0;
            int to = 0;

            if (!int.TryParse(args[0], out from) || !int.TryParse(args[1], out to))
            {
                //Console.WriteLine("Error:");
                //Console.WriteLine("Invalid arguments!");
                Environment.Exit(1);
            }

            //Stopwatch stopwatch = new Stopwatch();

            //stopwatch.Start();
            var res = PrimeChecker.GetMersenePrimesBetween(from, to);
            //stopwatch.Stop();
            //Console.WriteLine($"mersene primes between {from} and {to}:");

            Console.WriteLine(String.Join(",", res));

            //Console.WriteLine(stopwatch.ElapsedMilliseconds);

            //Console.ReadLine();
            Environment.Exit(0);
        }
    }
}
