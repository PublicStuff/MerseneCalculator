﻿using MerseneCalculator.VM;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace MerseneCalculator.Converters
{
    public class StatusColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is ProcessVMStatus status)
            {
                switch (status)
                {
                    case ProcessVMStatus.Running:
                        return Brushes.LightYellow;

                    case ProcessVMStatus.Stopped:
                        return Brushes.PaleVioletRed;

                    case ProcessVMStatus.Result:
                        return Brushes.YellowGreen;

                    case ProcessVMStatus.StoppedResult:
                        return Brushes.LightGreen;

                    case ProcessVMStatus.NotStarted:
                    default:
                        break;
                }
            }

            return Brushes.White;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
