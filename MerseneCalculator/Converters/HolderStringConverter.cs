﻿using MerseneCalculator.Processes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace MerseneCalculator.Converters
{
    public class HolderStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is ProcessPropertiesHolder holder)
            {
                return holder.ToString();
            }

            if (value is StatusUpdateHolder status)
            {
                return status.ToString();
            }

            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
