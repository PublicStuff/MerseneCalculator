﻿using MerseneCalculator.Processes;
using MerseneCalculator.VM;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace MerseneCalculator.Converters
{
    public class ResultStringConverter : IValueConverter
    {
        private static BigInteger two = new BigInteger(2);

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is MersenneResultVM resultVM && resultVM.Results != null)
            {
                return string.Join(", ", resultVM.Results/*.Select(num => BigInteger.Pow(two,num) - 1)*/);
            }

            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
