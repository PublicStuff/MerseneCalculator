﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MerseneCalculator.Commands
{
    public class Command : ICommand
    {
        private Action<object> executeAction;
        private Func<object, bool> canExecuteAction;
        //private bool canExecute;

        public event EventHandler CanExecuteChanged;

        public Command(Action<object> executeAction, Func<object,bool> canExecuteAction)
        {
            this.executeAction = executeAction;
            this.canExecuteAction = canExecuteAction;

            //this.canExecute = false;
        }

        public bool CanExecute(object parameter)
        {
            return this.canExecuteAction(parameter);
        }

        public void Execute(object parameter)
        {
            this.executeAction(parameter);
        }
    }
}
