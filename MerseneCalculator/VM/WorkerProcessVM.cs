﻿using MerseneCalculator.Processes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MerseneCalculator.VM
{
    public class WorkerProcessVM : INotifyPropertyChanged
    {
        private readonly WorkerProcess process;
        private ProcessVMStatus status;
        private ProcessPropertiesHolder properties;
        private StatusUpdateHolder statusInfos;
        //private List<int> results;
        
        public WorkerProcessVM(WorkerProcess process, ICommand removeCommand)
        {
            this.process = process;
            this.RemoveCommand = removeCommand;

            this.process.ProcessStarted += Process_ProcessStarted;
            this.process.ProcessStatusUpdate += Process_ProcessStatusUpdate;
            this.process.ProcessStopped += Process_ProcessStopped;
            this.process.ProcessResultReceived += Process_ProcessResultReceived;

            this.process.Start();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler<WorkerProcessVMResultReceivedEventArgs> ResultReceived;

        public ProcessVMStatus Status
        {
            get
            {
                return this.status;
            }

            private set
            {
                this.status = value;
                this.Notify(nameof(this.Status));
            }
        }

        public int From
        {
            get
            {
                return this.process.From;
            }
        }

        public int To
        {
            get
            {
                return this.process.To;
            }
        }

        public ICommand RemoveCommand
        {
            get;
        }

        //public List<int> Results
        //{
        //    get
        //    {
        //        return this.results;
        //    }

        //    private set
        //    {
        //        this.results = value;
        //        this.Notify(nameof(this.Results));
        //    }
        //}

        public ProcessPropertiesHolder Properties
        {
            get
            {
                return this.properties;
            }

            private set
            {
                this.properties = value;
                this.Notify(nameof(this.Properties));
            }
        }

        public StatusUpdateHolder StatusInfos
        {
            get
            {
                return this.statusInfos;
            }

            private set
            {
                this.statusInfos = value;
                this.Notify(nameof(this.StatusInfos));
            }
        }

        public void Stop()
        {
            try
            {
                this.process.Stop();
            }
            catch (ArgumentNullException)
            {
            }

            // Todo: unsunscribe from events, but not now
        }

        protected virtual void Notify([CallerMemberName] string propName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        protected virtual void FireResultReceived(object sender, WorkerProcessVMResultReceivedEventArgs e)
        {
            this.ResultReceived?.Invoke(sender, e);
        }

        private void Process_ProcessStopped(object sender, ProcessStoppedEventArgs e)
        {
            if (e != null)
            {
                if (this.Status == ProcessVMStatus.Result || this.Status == ProcessVMStatus.StoppedResult)
                {
                    this.Status = ProcessVMStatus.StoppedResult;
                }
                else
                {
                    this.Status = ProcessVMStatus.Stopped;
                }
            }

            //todo: maybe delete yourself
        }

        private void Process_ProcessStatusUpdate(object sender, ProcessStatusEventArgs e)
        {
            if (e != null)
            {
                switch (e.UpdateType)
                {
                    case UpdateType.Properties:
                        try
                        {
                            this.Properties = (ProcessPropertiesHolder)e.UpdateHolder;
                        }
                        catch (InvalidCastException)
                        {
                        }

                        break;
                    case UpdateType.StatusUpdate:
                        try
                        {
                            this.StatusInfos = (StatusUpdateHolder)e.UpdateHolder;
                        }
                        catch (InvalidCastException)
                        {
                        }

                        break;
                    default:
                        throw new NotImplementedException();
                }
            }
        }

        private void Process_ProcessStarted(object sender, ProcessStartedEventArgs e)
        {
            if (e != null)
            {
                if (this.Status == ProcessVMStatus.NotStarted)
                {
                    this.Status = ProcessVMStatus.Running;
                }
            }
        }

        private void Process_ProcessResultReceived(object sender, ProcessResultEventArgs e)
        {
            if (e != null)
            {
                if (e.Sucess && e.Results.Results != null)
                {
                    this.Status = ProcessVMStatus.Result;
                    this.FireResultReceived(this, new WorkerProcessVMResultReceivedEventArgs(e.Results));
                }
                else
                {
                    this.Status = ProcessVMStatus.Stopped;
                }
            }
        }
    }
}
