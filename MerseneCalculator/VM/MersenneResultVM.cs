﻿using MerseneCalculator.Processes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MerseneCalculator.VM
{
    public struct MersenneResultVM
    {
        private readonly MersenneResult result;

        public MersenneResultVM(MersenneResult result)
        {
            this.result = result;
        }

        public int From
        {
            get
            {
                return this.result.From;
            }
        }

        public int To
        {
            get
            {
                return this.result.To;
            }
        }

        public List<int> Results
        {
            get
            {
                return this.result.Results.ToList();
            }
        }
    }
}
