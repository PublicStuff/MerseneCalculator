﻿namespace MerseneCalculator.VM
{
    public enum ProcessVMStatus
    {
        NotStarted,
        Running,
        Stopped,
        Result,
        StoppedResult
    }
}
