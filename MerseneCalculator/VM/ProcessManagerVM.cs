﻿using MerseneCalculator.Commands;
using MerseneCalculator.Processes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;

namespace MerseneCalculator.VM
{
    public class ProcessManagerVM : INotifyPropertyChanged
    {
        private static object locker = new object();
        private ICommand removeCommand;
        private ObservableCollection<WorkerProcessVM> processList;
        private ObservableCollection<MersenneResultVM> resultsList;

        public ProcessManagerVM()
        {
            this.ProcessList = new ObservableCollection<WorkerProcessVM>();

            this.ResultsList = new ObservableCollection<MersenneResultVM>();
            BindingOperations.EnableCollectionSynchronization(this.ResultsList, locker);

            this.SetupCommands();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand AddProcessCommand
        {
            get;
            private set;
        }

        public Command KillAllCommand
        {
            get;
            private set;
        }

        public ObservableCollection<MersenneResultVM> ResultsList
        {
            get
            {
                return this.resultsList;
            }

            private set
            {
                this.resultsList = value;
                this.Notify(nameof(this.ResultsList));
            }
        }


        public ObservableCollection<WorkerProcessVM> ProcessList
        {
            get
            {
                return this.processList;
            }

            private set
            {
                this.processList = value;
                this.Notify(nameof(this.ProcessList));
            }
        }

        public int InputFrom
        {
            get;
            set;
        }

        public int InputTo
        {
            get;
            set;
        }

        protected virtual void Notify([CallerMemberName]string propName = null)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));

            var asd = new StatusUpdateHolder();

            asd.HasExited = true;
        }

        internal void KillAll()
        {
            if (this.ProcessList != null)
            {
                foreach (var proc in this.ProcessList)
                {
                    proc.ResultReceived -= this.ProcessVM_ResultReceived;
                    proc.Stop();
                }
            }
        }

        private void SetupCommands()
        {
            this.removeCommand = new Command(
                new Action<object>(obj =>
                {
                    if (obj is WorkerProcessVM processVM)
                    {
                        processVM.Stop();
                        processVM.ResultReceived -= this.ProcessVM_ResultReceived;
                        this.ProcessList.Remove(processVM);
                    }
                }),
                new Func<object, bool>(obj =>
                {
                    //return obj is WorkerProcessVM processVM && this.ProcessList.Contains(processVM);
                    return true;
                }));

            this.AddProcessCommand = new Command(
                new Action<object>(obj =>
                {
                    WorkerProcessVM processVM = new WorkerProcessVM(
                        new WorkerProcess(this.InputFrom, this.InputTo),
                        this.removeCommand);

                    processVM.ResultReceived += this.ProcessVM_ResultReceived;

                    this.ProcessList.Add(processVM);
                }),
                new Func<object, bool>(obj =>
                {
                    return this.ProcessList != null;
                }));

            //this.KillAllCommand = new Command(
            //    new Action<object>(obj =>
            //    {
            //        foreach (var proc in this.ProcessList)
            //        {
            //            proc.Stop();
            //        }
            //    }),
            //    new Func<object, bool>(obj =>
            //    {
            //        return this.ProcessList != null;
            //    }));
        }

        private void ProcessVM_ResultReceived(object sender, WorkerProcessVMResultReceivedEventArgs e)
        {
            if (e != null)
            {
                //lock (this.ResultsList)
                //{
                    this.ResultsList.Add(new MersenneResultVM(e.Result));
                //}
            }
        }
    }
}
