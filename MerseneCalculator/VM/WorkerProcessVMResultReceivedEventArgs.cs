﻿using MerseneCalculator.Processes;
using System;

namespace MerseneCalculator.VM
{
    public class WorkerProcessVMResultReceivedEventArgs : EventArgs
    {
        public WorkerProcessVMResultReceivedEventArgs(MersenneResult result)
        {
            this.Result = result;
        }

        public MersenneResult Result
        {
            get;
        }
    }
}