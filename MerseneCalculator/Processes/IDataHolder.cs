﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MerseneCalculator.Processes
{
    public interface IDataHolder
    {
        string ToString();
    }
}
