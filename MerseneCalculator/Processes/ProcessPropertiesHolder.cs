﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MerseneCalculator.Processes
{
    public class ProcessPropertiesHolder : IDataHolder
    {
        public int BasePrioroty
        {
            get;
            set;
        }

        public int Id
        {
            get;
            set;
        }

        public string MainModule
        {
            get;
            set;
        }

        public string MainWindowTitle
        {
            get;
            set;
        }

        public string Modules
        {
            get;
            set;
        }

        public bool PriorityBoostEnabled
        {
            get;
            set;
        }

        public string PriorityClass
        {
            get;
            set;
        }

        public string ProcessName
        {
            get;
            set;
        }

        public string ProzessorAffinity
        {
            get;
            set;
        }

        public DateTime StartTime
        {
            get;
            set;
        }
        
        public override string ToString()
        {
            string res = string.Empty;

            try
            {
                res += $"Id: {this.Id}\n";
                res += $"Process name: {this.ProcessName}\n";
                res += $"Main module: {this.MainModule}\n";
                res += $"Main window title: {this.MainWindowTitle}\n";
                res += $"Modules: {this.Modules}\n";
                res += $"Base priority: {this.BasePrioroty}\n";
                res += $"Priority-boost enabled: {this.PriorityBoostEnabled}\n";
                res += $"Priority-class: {this.PriorityClass} MB\n";
                res += $"Processor affinity: {this.ProzessorAffinity}\n";
                res += $"Start time: {this.StartTime}\n";
            }
            catch (ArgumentNullException)
            {
            }

            return res;
        }
    }
}
