﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MerseneCalculator.Processes
{
    public class WorkerProcess
    {
        private Task watcherTask;
        private Process process;
        private PerformanceCounter cpuPerformanceCounter;
        private PerformanceCounter ramPerformanceCounter;
        private static readonly string path;
        private readonly int sleepTime;
        private bool exit;

        static WorkerProcess()
        {
            path = Path.Combine(Directory.GetCurrentDirectory(), @"..\..\..\MerseneWorker\bin\Debug\MerseneWorker.exe");
        }

        public WorkerProcess(int from, int to, int sleepTime = 500)
        {
            this.From = from;
            this.To = to;
            this.sleepTime = sleepTime;
        }

        public event EventHandler<ProcessStartedEventArgs> ProcessStarted;
        public event EventHandler<ProcessStoppedEventArgs> ProcessStopped;
        public event EventHandler<ProcessStatusEventArgs> ProcessStatusUpdate;
        public event EventHandler<ProcessResultEventArgs> ProcessResultReceived;

        public int From
        {
            get;
            private set;
        }

        public int To
        {
            get;
            set;
        }


        public bool IsRunning
        {
            get
            {
                return this.watcherTask != null
                    && this.watcherTask.Status == TaskStatus.Running
                    && this.IsProcessRunning();
            }
        }

        public ProcessPropertiesHolder Properties
        {
            get;
        }

        public void Start()
        {
            if (this.IsRunning)
            {
                return;
            }

            ProcessStartInfo procinfo = new ProcessStartInfo(path)
            {
                CreateNoWindow = true,
                WindowStyle = ProcessWindowStyle.Hidden,
                RedirectStandardError = true,
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                UseShellExecute = false,
                Arguments = $"{this.From} {this.To}"
            };

            this.process = new Process()
            {
                StartInfo = procinfo
            };

            this.process.OutputDataReceived += this.Process_OutputDataReceived;
            this.process.Start();

            if (procinfo.RedirectStandardOutput)
            {
                process.BeginOutputReadLine();

                this.watcherTask = new Task(this.Worker);

                try
                {
                    this.watcherTask.Start();
                }
                catch
                {
                    this.watcherTask = null;

                    this.process.OutputDataReceived -= this.Process_OutputDataReceived;

                    try
                    {
                        this.process.Kill();
                    }
                    catch
                    {
                    }
                }
            }
            else
            {
                this.process.OutputDataReceived -= this.Process_OutputDataReceived;

                try
                {
                    this.process.Kill();
                }
                catch
                {
                }                
            }
        }

        public void Stop()
        {
            if (!this.IsRunning)
            {
                return;
            }

            this.exit = true;

            this.process.OutputDataReceived -= this.Process_OutputDataReceived;

            try
            {
                this.process.Kill();
            }
            catch
            {
            }

            // Todo: handle multiple firing
            this.FireStopped(this, new ProcessStoppedEventArgs());
        }
        
        protected virtual void FireStatusUpdate(object sender, ProcessStatusEventArgs e)
        {
            this.ProcessStatusUpdate?.Invoke(this, e);
        }

        protected virtual void FireStarted(object sender, ProcessStartedEventArgs e)
        {
            this.ProcessStarted?.Invoke(this, e);
        }

        protected virtual void FireStopped(object sender, ProcessStoppedEventArgs e)
        {
            this.ProcessStopped?.Invoke(this, e);
        }

        protected virtual void FireResultReceived(object sender, ProcessResultEventArgs e)
        {
            this.ProcessResultReceived?.Invoke(this, e);
        }

        private bool IsProcessRunning()
        {
            try
            {
                //return Process.get.GetProcessesByName(this.process.ProcessName).Any();

                return !Process.GetProcessById(this.process.Id).HasExited;
            }
            catch
            {
                return false;
            }
        }

        private void Process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            this.exit = true;

            if (e.Data == null)
            {
                // Todo: handle multiple firing
                this.FireStopped(this, new ProcessStoppedEventArgs());
            }

            IEnumerable<int> result = null;

            try
            {
                result = e.Data.Split(new[] { ' ', ',' }, StringSplitOptions.RemoveEmptyEntries).Select(numString => int.Parse(numString));
                this.FireResultReceived(this, new ProcessResultEventArgs(true, new MersenneResult(this.From, this.To, result)));
            }
            catch
            {
                // Todo: handle multiple firing
                this.FireStopped(this, new ProcessStoppedEventArgs());
            }

            // Todo: handle multiple firing
            this.FireStopped(this, new ProcessStoppedEventArgs());
        }


        private void Worker()
        {
            if (!this.SetupPerformanceCounter())
            {
                this.Stop();
            }

            this.FireStarted(this, new ProcessStartedEventArgs());

            // todo: return initial data which not changes somehow
            if (this.GetStaticProcessData(out ProcessPropertiesHolder properties))
            {
                this.FireStatusUpdate(this, new ProcessStatusEventArgs(UpdateType.Properties, properties));
            }

            this.FireStatusUpdate(this, new ProcessStatusEventArgs(UpdateType.Properties, new ProcessPropertiesHolder()));

            while (!this.exit)
            {
                //Todo: read and store all properties here, then fire update event
                if (this.GetProcessUpdateData(out StatusUpdateHolder updates))
                {
                    this.FireStatusUpdate(this, new ProcessStatusEventArgs(UpdateType.StatusUpdate, updates));
                }

                if (!this.IsProcessRunning())
                {
                    this.Stop();
                }

                Task.Delay(this.sleepTime).Wait();
            }

            // todo: maybe fire stopped here to..
            //this.FireStopped()
        }

        private bool GetProcessUpdateData(out StatusUpdateHolder holder)
        {    
            try
            {
                holder = new StatusUpdateHolder()
                {
                    CpuPercent = Math.Round(this.cpuPerformanceCounter.NextValue() / Environment.ProcessorCount, 2),
                    RamPercent = Math.Round(this.ramPerformanceCounter.NextValue() / 1024 / 1024, 2),
                    HasExited = this.process.HasExited,
                    NonPagedMemorySize = this.process.NonpagedSystemMemorySize64,
                    PagedMemorySize = this.process.PagedMemorySize64,
                    PagedSystemMemorySize = this.process.PagedSystemMemorySize64,
                    PeakPagedMemorySize = this.process.PeakPagedMemorySize64,
                    PeakVirtualMemorySize = this.process.PeakVirtualMemorySize64,
                    PeakWorkingSet64 = this.process.PeakWorkingSet64,
                    PrivateMemorySize = this.process.PrivateMemorySize64,
                    PrivilegedProcessorTime = this.process.PrivilegedProcessorTime,
                    Responding = this.process.Responding,
                    ThreadsCount = this.process.Threads.Count,
                    TotalProcessorTime = this.process.TotalProcessorTime,
                    UserProcessorTime = this.process.UserProcessorTime,
                    WorkingSet64 = this.process.WorkingSet64,
                    VirtualMemorySize64 = this.process.VirtualMemorySize64
                };

            }
            catch
            {
                holder = new StatusUpdateHolder();
                return false;
            }

            return true;
        }

        private bool GetStaticProcessData(out ProcessPropertiesHolder holder)
        {
            try
            {
                holder = new ProcessPropertiesHolder()
                {
                    BasePrioroty = this.process.BasePriority,
                    Id = this.process.Id,
                    MainModule = this.process.MainModule.ModuleName,
                    MainWindowTitle = this.process.MainWindowTitle,
                    Modules = string.Join(", ", this.process.Modules),
                    PriorityBoostEnabled = this.process.PriorityBoostEnabled,
                    ProcessName = this.process.ProcessName,
                    PriorityClass = this.process.PriorityClass.ToString(),
                    ProzessorAffinity = this.process.ProcessorAffinity.ToInt64().ToString(),
                    StartTime = this.process.StartTime
                };
            }
            catch
            {
                holder = new ProcessPropertiesHolder();
                return false;
            }

            return true;
        }

        private bool SetupPerformanceCounter()
        {
            // Todo: check whether it really finds the right name, if not use this code
            //var name = string.Empty;

            //foreach (var instance in new PerformanceCounterCategory("Process").GetInstanceNames())
            //{
            //    if (instance.StartsWith(this.process.ProcessName))
            //    {
            //        using (var processId = new PerformanceCounter("Process", "ID Process", instance, true))
            //        {
            //            if (process.Id == (int)processId.RawValue)
            //            {
            //                name = instance;
            //                break;
            //            }
            //        }
            //    }
            //}
            try
            {
                this.cpuPerformanceCounter = new PerformanceCounter("Process", "% Processor Time", this.process.ProcessName, true);
                this.ramPerformanceCounter = new PerformanceCounter("Process", "Private Bytes", this.process.ProcessName, true);
                this.cpuPerformanceCounter.NextValue();
                this.ramPerformanceCounter.NextValue();
            }
            catch
            {
                return false;
            }

            return true;
        }
    }
}
