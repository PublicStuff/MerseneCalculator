﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace MerseneCalculator.Processes
{
    public class StatusUpdateHolder : IDataHolder
    {
        public StatusUpdateHolder()
        {
        }

        public double CpuPercent
        {
            get;
            set;
        }

        public double RamPercent
        {
            get;
            set;
        }

        public int ExitCode
        {
            get;
            set;
        }

        public System.DateTime ExitTime
        {
            get;
            set;
        }

        public bool HasExited
        {
            get;
            set;
        }

        public long NonPagedMemorySize
        {
            get;
            set;
        }

        public long PagedMemorySize
        {
            get;
            set;
        }

        public long PagedSystemMemorySize
        {
            get;
            set;
        }

        public long PeakPagedMemorySize
        {
            get;
            set;
        }

        public long PeakVirtualMemorySize
        {
            get;
            set;
        }

        public long PeakWorkingSet64
        {
            get;
            set;
        }

        public long PrivateMemorySize
        {
            get;
            set;
        }

        public TimeSpan PrivilegedProcessorTime
        {
            get;
            set;
        }

        public TimeSpan TotalProcessorTime
        {
            get;
            set;
        }

        public TimeSpan UserProcessorTime
        {
            get;
            set;
        }

        public bool Responding
        {
            get;
            set;
        }

        // Instead of processor threadcollection
        public int ThreadsCount
        {
            get;
            set;
        }

        public long VirtualMemorySize64
        {
            get;
            set;
        }

        public long WorkingSet64
        {
            get;
            set;
        }

        public override string ToString()
        {
            string res = string.Empty;

            try
            {
                res += $"CPU-percentage: {this.CpuPercent}%\n";
                res += $"RAM-percentage: {this.RamPercent}%\n";
                res += $"Exitcode: {this.ExitCode}\n";
                res += $"Exit time: {this.ExitTime.ToLongTimeString()}\n";
                res += $"Has exited: {this.HasExited}\n";
                res += $"Non-paged memory size: {Math.Round((double)this.NonPagedMemorySize / 1024 / 1024, 2)} MB\n";
                res += $"Paged memory size: {Math.Round((double)this.PagedMemorySize / 1024 / 1024, 2)} MB\n";
                res += $"Paged system memory size: {Math.Round((double)this.PagedSystemMemorySize / 1024 / 1024, 2)} MB\n";
                res += $"Peak paged memory size: {Math.Round((double)this.PeakPagedMemorySize / 1024 / 1024, 2)} MB\n";
                res += $"Peak virtual memory size: {Math.Round((double)this.PeakVirtualMemorySize / 1024 / 1024, 2)} MB\n";
                res += $"Peak working set: {Math.Round((double)this.PeakWorkingSet64 / 1024 / 1024, 2)} MB\n";
                res += $"Private memory size: {Math.Round((double)this.PrivateMemorySize / 1024 / 1024, 2)} MB\n";
                res += $"Virtual memory size: {Math.Round((double)this.VirtualMemorySize64 / 1024 / 1024, 2)} MB\n";
                res += $"Working set: {Math.Round((double)this.WorkingSet64 / 1024 / 1024, 2)} MB\n";
                res += $"Privileged processor time: {this.PrivilegedProcessorTime.Seconds} s\n";
                res += $"User processor time: {this.UserProcessorTime.Seconds} s\n";
                res += $"Total processor time: {this.TotalProcessorTime.Seconds} s\n";
                res += $"Responding: {this.Responding}\n";
                res += $"Threads count: {this.ThreadsCount}\n";
            }
            catch (ArgumentNullException)
            {
            }

            return res;
        }
    }
}