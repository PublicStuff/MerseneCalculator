﻿using System;

namespace MerseneCalculator.Processes
{
    public  class ProcessStartedEventArgs : EventArgs
    {
        public ProcessStartedEventArgs()
        {
            this.Timestamp = DateTime.Now;
        }

        public DateTime Timestamp
        {
            get;
        }
    }
}