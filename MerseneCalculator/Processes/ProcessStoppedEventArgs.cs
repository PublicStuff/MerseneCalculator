﻿using System;
using System.Collections.Generic;

namespace MerseneCalculator.Processes
{
    public class ProcessStoppedEventArgs : EventArgs
    {
        public ProcessStoppedEventArgs()
        {
            this.Timestamp = DateTime.Now;
        }

        public DateTime Timestamp
        {
            get;
        }
    }
}