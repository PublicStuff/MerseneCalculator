﻿using System;
using System.Collections.Generic;

namespace MerseneCalculator.Processes
{
    public class ProcessResultEventArgs : EventArgs
    {
        public ProcessResultEventArgs(bool sucess, MersenneResult result)
        {
            this.Sucess = sucess;
            this.Results = result;
        }

        public bool Sucess
        {
            get;
            private set;
        }

        public MersenneResult Results
        {
            get;
            private set;
        }
    }
}