﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MerseneCalculator.Processes
{
    public struct MersenneResult
    {
        public MersenneResult(int from, int to, IEnumerable<int> results)
        {
            this.From = from;
            this.To = to;
            this.Results = results;
        }

        public int From
        {
            get;
        }

        public int To
        {
            get;
        }

        public IEnumerable<int> Results
        {
            get;
        }
    }
}
