﻿using System;

namespace MerseneCalculator.Processes
{
    public  class ProcessStatusEventArgs : EventArgs
    {
        public ProcessStatusEventArgs(UpdateType type, IDataHolder status)
        {
            this.UpdateType = type;
            this.UpdateHolder = status;
        }

        public UpdateType UpdateType { get; }

        public IDataHolder UpdateHolder { get; }
    }
}